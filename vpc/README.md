# PLEASE USE FINNAIR GITHUB INSTEAD
# https://github.com/FinnairOyj/cops-aws-tools/tree/dcmigration/templates/cloudformation/dc-migration
## NOTE THE BRANCH IS dcmigration NOT master


## Basic VPC

- VPC with 2 private subnets in 2 AZ
- Option for 2 secondary CIDR ranges that can be added/removed without recreating VPC
- S3 gatway endpoint
- Option for VPC interface endpoints required for SSM
- Option for Route53 privare zone

![vpc-diagram](basicvpc.png)

## VPC for CloudEndure migration

- VPC with staging, testing and productions subnets in single AZ
- Decicated routing tables for each subnet
- Option for 2 secondary CIDR ranges that can be added/removed without recreating VPC
- S3 gateway endpoint
- Option for VPC interface endpoints required for SSM
- Option for Route53 private zone

NOTE: When removing CIDR ranges you should first remove subnets and then do second update for CIDR range removal. Depedency between CIRD range and subnet is one-way only so CIDR ranges will be created before subnets but removal can happen in wrong order and fail to remove CIDR ranges as subnets are still present.

![vpc-diagram](dcvpc.png)

