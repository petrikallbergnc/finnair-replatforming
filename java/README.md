# PLEASE USE FINNAIR GITHUB INSTEAD
# https://github.com/FinnairOyj/cops-aws-tools/tree/dcmigration/templates/cloudformation/dc-migration
## NOTE THE BRANCH IS dcmigration NOT master

## Generic Java applications on AWS

- What are the application in this category?
- Is the architecture single service as jar -file or something more complex?
- Assuming there needs to be db (RDS) and persistent filesystem storage (EFS)
- Can you run multiple copies in parallel? (Or is there a need for scaling?)
- Are these application in active development? Or is there plans to build more of these?

Proposed architecture:

- ECS (for simplicity over EKS) Fargate (for no managed hosts).
- ECR (container repo as service) with regular image scanning enabled.
- RDS and EFS for persistent storage. EFS support in Cloudformation
for ECS Fargate is coming soon. See AWS Container Roadmap.
- ALB for fixed end-point.
- Cloudwatch Logs for application logs.
- Cloudwatch monitoring baseline; ECS service scaling, Filter for errors in logs,
healthy / unhealthy backends for ALB, ALB backend errors, ECS task cpu usage ...
- Process to package application into container image and deploy to ECS.
Level of automation depends on how active development is. Think also
how local testing would be done.
- Where is DNS going to be? If not in local Route53 (=same account as application)
how is DNS records and SSL certificates (ACM) going to be created/updated.
