# PLEASE USE FINNAIR GITHUB INSTEAD
# https://github.com/FinnairOyj/cops-aws-tools/tree/dcmigration/templates/cloudformation/dc-migration
## NOTE THE BRANCH IS dcmigration NOT master

## Database replatforming to AWS RDS

- Is the plan to migrate 1:1 between db:rds or db-server:rds?
  (ie continue using shared db servers or not?)
- Syncing data between IBM DC and AWS RDS? Connectivity and appliances etc.
- Re-routing clients to AWS RDS? How clients find/connect right RDS instance?
- Sizing and HA requirements?
- Backups?

NOTE: Above is just questions that are going to effect AWS design. There are
people from db migration team assigned to project who can define migration
plan and define requirements for infrastructure.
( katarzyna.piegat@nordcloud.com , mark.sherwood@nordcloud.com )

