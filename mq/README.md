# PLEASE USE FINNAIR GITHUB INSTEAD
# https://github.com/FinnairOyj/cops-aws-tools/tree/dcmigration/templates/cloudformation/dc-migration
## NOTE THE BRANCH IS dcmigration NOT master

## IBM MQ on AWS

- Single instance ASG (on 2 AZ) for IBM MQ server
- Network Load Balancer with TCP listeneres on ports 1414 and 9443
- EFS file system for persistent storage
- Cloudwatch alerting (via pre-created SNS topic) for
    - Auto-scaling errors; EC2_INSTANCE_LAUNCH_ERROR, EC2_INSTANCE_TERMINATE_ERROR
    - Over 90% CPU usage
    - More than 0 unhealthy hosts
    - Less than 1 healthy hosts
- OS level access via SSM Session Manager (ie. no bastion host needed)
- Server is responding on port 1414 (mq) and 9443 (web console and api)

### TESTING & DEBUGGING

Open a console session on MQ instance

```
$ aws ssm start-session --target MQ-INSTANCE-ID
```

Connect MQ web console, forward your local port to instance port 9443
and then open a browser to https://localhost:9443/ibmmq/console/ 
This is bit slow to respond, might be some configuration issue that
needs further investigation. Note that this doesn't go via LB but
directly to instance, skipping all security groups and LB configs.

```
$ aws ssm start-session --target MQ-INSTANCE-ID \
--document-name AWS-StartPortForwardingSession \
--parameters '{"portNumber":["9443"],"localPortNumber":["9443"]}'
```

### TODO

- EFS deletion policy should be retaining the data?
- Where is Route53? Should there be DNS name created for load balancer?
- Is it possible to run 2 instances in active/standby -config and MQ would take care of failover?
Or maybe something like descripbed in https://developer.ibm.com/recipes/tutorials/architecting-messaging-solutions-in-aws-cloud-using-ibm-mq/
This is also describing how to install MQ on linux that might be a good
idea as I'm not sure how trustworthy the image from AWS Quickstart is.
At very least the image should be copied to Finnair's AWS account.
Maybe it would be a good idea to do custom (secure) AMI build for this?
- Would it be possible to do migration from IBM data center to AWS by
joining the existing MQ cluster (if there is one?) and then finally
just shuting down instances in data center when migration is ready?
- If Finnair is planning to deploy services on both eu-central-1 and eu-north-1
should there be MQ cluster nodes on both regions (talking over peered tgws)?

![aws-diagram](ibm-mq.png)

See Original Quickstart "IBM MQ on AWS" at

* https://aws.amazon.com/quickstart/architecture/ibm-mq/
* https://github.com/aws-quickstart/quickstart-ibm-mq
